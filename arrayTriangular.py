def menu():
    global array1,array2,jOrdo
    print('------------------------------------------------------')
    print 'Segitiga Atas & Bawah'
    print('------------------------------------------------------')
    print('Pilihan Tipe Array Segitiga')
    print('1. Tipe 1')
    print('2. Tipe 2')
    print('3. Tipe 3')
    pilihan=input('Pilih tipe array[1-3] : ')
    print('------------------------------------------------------')
    jOrdo=input('Masukkan jumlah ordo: ')
    print('------------------------------------------------------')
    array1=[[]for i in range(jOrdo)]
    array2=[[]for i in range(jOrdo)]
    for i in range(0,jOrdo):
            for j in range(0,jOrdo):
                array1[i].append("0")
                array2[i].append("0")
    if pilihan==1:
        tipe1()
    if pilihan==2:
        tipe2()
    if pilihan==3:
        tipe3()
def tipe1():
    global array1,array2,jOrdo
    for i in range(0,jOrdo):
        for j in range(0,jOrdo):
            if i<=j:
                print 'Masukkan elemen segitiga atas [',i+1,',',j+1,']',
                isi=raw_input(': ')
                array1[i][j]=isi
            else:
                array1[i][j]='0'
    print
    for i in range(0,jOrdo):
        for j in range(0,jOrdo):
            if i<=j:
                array2[i][j]='0'
            else:
                print 'Masukkan elemen segitiga bawah [',i+1,',',j+1,']',
                isi=raw_input(': ')
                array2[i][j]=isi
    print
    print('Segitiga Atas')
    print('------------------------------------------------------')
    for i in range(0,jOrdo):
        for j in range(0,jOrdo):
            print array1[i][j],
        print
    print
    print('Segitiga Bawah')
    print('------------------------------------------------------')
    for i in range(1,jOrdo):
        for j in range(0,jOrdo-1):
            print array2[i][j],
        print
    print
    print "Array Segitiga Tipe 1"
    print('------------------------------------------------------')
    for i in range(0,jOrdo):
        for j in range(0,jOrdo):
            if i<=j :
                print array1[i][j],
            else:
                print array2[i][j],
        print
    print
    menu()
def tipe2():
    global array1,array2,jOrdo    
    for i in range(0,jOrdo):
        for j in range(0,jOrdo):
            if i<=j:
                print 'Masukkan elemen segitiga atas [',i+1,',',j+1,']',
                isi=raw_input(': ')
                array1[i][j]=isi
            else:
                array1[i][j]='0'
    print
    for i in range(0,jOrdo):
        for j in range(0,jOrdo):
            if i>=j:
                print 'Masukkan elemen segitiga bawah [',i+1,',',j+1,']',
                isi=raw_input(': ')
                array2[i][j]=isi
            else:
                array2[i][j]='0'
    print
    print('Segitiga Atas')
    print('------------------------------------------------------')
    for i in range(0,jOrdo):
        for j in range(0,jOrdo):
            print array1[i][j],
        print
    print
    print('Segitiga Bawah')
    print('------------------------------------------------------')
    for i in range(0,jOrdo):
        for j in range(0,jOrdo):
            print array2[i][j],
        print
    print
    print "Array Segitiga Tipe 2"
    print('------------------------------------------------------')
    for i in range(0,jOrdo):
        for j in range(0,jOrdo+1):
            if i>=j :
                print array2[i][j],
            else:
                print array1[i][j-1],
        print
    print
    menu()
def tipe3():
    global array1,array2,jOrdo
    for i in range(0,jOrdo):
        for j in range(0,jOrdo):
            if i>=j:
                print 'Masukkan Elemen Segitiga Pertama [',i+1,',',j+1,']',
                isi=raw_input(': ')
                array1[i][j]=isi
            else:
                array1[i][j]='0'
    print
    for i in range(0,jOrdo):
        for j in range(0,jOrdo):
            if i>=j:
                print 'Masukkan Elemen Segitiga Kedua [',i+1,',',j+1,']',
                isi=raw_input(': ')
                array2[i][j]=isi
            else:
                array2[i][j]='0'
    print
    print('Segitiga 1')
    print('------------------------------------------------------')
    for i in range(0,jOrdo):
        for j in range(0,jOrdo):
            print array1[i][j],
        print
    print
    print('Segitiga 2')
    print('------------------------------------------------------')
    for i in range(0,jOrdo):
        for j in range(0,jOrdo):
            print array2[i][j],
        print
    print
    print "Pilih Yang Ingin Ditranspose" 
    print "1. Segitiga 1" 
    print "2. Segitiga 2"
    print
    pilTranspose=input("Pilihan Anda [1-2]:")
    if pilTranspose==1:    
        print "Array Segitiga"
        print('------------------------------------------------------')
        for i in range(0,jOrdo):
            for j in range(0,jOrdo+1):
                if i>=j:
                    print array2[i][j],
                else:
                    print array1[j-1][i],
            print
        print
    if pilTranspose==2:    
        print "Array Segitiga"
        print('------------------------------------------------------')
        for i in range(0,jOrdo):
            for j in range(0,jOrdo+1):
                if i>=j:
                    print array1[i][j],
                else:
                    print array2[j-1][i],
            print
        print
    menu()
menu()