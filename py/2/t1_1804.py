def menu():
    print 'Daftar Menu'
    print '---------------'
    print '1.Persegi'
    print '2.Lingkaran'
    print '3.Segitiga'
    print '4.Keluar'
    print '---------------'


def persegi():
    print 'Menghitung Luas Persegi Panjang'
    print '-------------------------------'
    print
    p=input('Masukkan panjang :')
    l=input('Masukkan lebar :')
    luaspp=p*l
    print 'Luas persegi panjang =',luaspp
    print
    print 'Ingin coba hitung luas lagi[y/t]:'
    lagi=raw_input().upper()
    if lagi=='Y':
        menu()
    else:
        exit()

def lingkaran():
    print 'Menghitung Luas Lingkaran'
    print '-------------------------------'
    print
    phi=3.14
    r=input('Masukkan jari-jari :')
    luasling=phi*r*r
    print 'Luas persegi panjang =',luasling
    print
    print 'Ingin coba hitung luas lagi[y/t]:'
    lagi=raw_input().upper()
    if lagi=='Y':
        menu()
    else:
        exit()

def segitiga():
    print 'Menghitung Luas Segitiga'
    print '-------------------------------'
    print
    a=input('Masukkan Alas :')
    t=input('Masukkan Tinggi :')
    luassegitiga=a*t
    print 'Luas persegi panjang =',luassegitiga
    print
    print 'Ingin coba hitung luas lagi[y/t]:'
    lagi=raw_input().upper()
    if lagi=='Y':
        menu()
    else:
        exit()

print
print 'Menu Menghitung Luas'
print '--------------------'
menu()
print
while 1:
    pil=input('Masukkan pilihan anda[1-4]:')
    if pil==1:
        persegi()
    elif pil==2:
        lingkaran()
    elif pil==3:
        segitiga()
    elif pil==4:
        '\n'*10
        break
    else:
        print 'Pilihan tidak ada di menu'
        print
        print 'Ingin coba hitung luas lagi[y/t]:'
        lagi=raw_input().upper()
        if lagi=='Y':
            menu()
        else:
            '\n'*10
            break
