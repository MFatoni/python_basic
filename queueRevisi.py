import os

array=[]
lastArrayP=0
front=0
rear=0
panjangQ=0

def menu():
    print 'Program Queue'
    print '---------------------------'
    print '1.Create\n2.Insert\n3.Remove\n4.Noel\n5.Cek Antrian\n0.Exit'
    try:    
        pilihan=input('Pilih Operasi [1-5]: ')
    except (NameError,SyntaxError):
        print 'Salah Karena Bukan Angka'
        print
        raw_input('Periksa Inputan')
        os.system('cls')
        menu()
    os.system('cls')
    if(pilihan==1):
        create()
    elif(pilihan==2):
        insert()
    elif(pilihan==3):
        remove()
    elif(pilihan==4):
        noel()
    elif(pilihan==5):
        check()
    elif(pilihan==0):
        exit()
    else:
        print 'Mohon Periksa Inputan'

def create():
    global panjangQ,front,rear,lastArrayP
    if(len(array)==0):
        try:
            panjangQ=input('input panjang queue :')
        except (NameError,SyntaxError):
            print 'Salah Karena Bukan Angka'
            print
            raw_input('Periksa Inputan')
            os.system('cls')
            create()
        for i in range(panjangQ):
            array.append("")
        print
        print 'Array Sukses Dibuat'
        raw_input('Tekan Sembarang Tombol Untuk Melanjutkan')
        os.system('cls')
        
        #variabel lokal
        batasan1=0
        batasan2=panjangQ

        inputFR=raw_input('Apakah Anda Ingin Menentukan Front Dan Rear Terlebih Dahulu[Y/T] :')
        if inputFR.upper()=='Y':
            print '----------------------------------------------------------------------------'
            front=input('Masukkan Posisi Front : ')
            isifront=raw_input('Masukkan Elemen Front : ')
            array[front-1]=isifront
            print
            rear=input('Masukkan Posisi Rear  : ')
            isirear=raw_input('Masukkan Elemen Rear  : ')
            array[rear-1]=isirear
            print '----------------------------------------------------------------------------'
            print 'Posisi Antrian Sebelum Dilakukan Penginputan'
            print '----------------------------------------------------------------------------'
            print array[0:panjangQ]
            print "Front : ", front ,',', array[front-1],"\nRear  : ", rear,',',array[rear-1]
            print '----------------------------------------------------------------------------'

            #proses perbandingan
            selisih=rear-front
            mRear=rear
            mFront=front
            
            if selisih>1:
                for mFront in range(mFront+1,mRear,1):
                    print 'Masukkan Elemen Ke-',mFront,' : ',
                    inputan=raw_input()
                    array[mFront-1]=inputan
            if selisih<0:
                for mFront in range(mFront+1,batasan2+1,1):
                    print 'Masukkan Elemen Ke-',mFront,' : ',
                    inputan=raw_input()
                    array[mFront-1]=inputan
                print
                for batasan1 in range(batasan1+1,mRear,1):
                    print 'Masukkan Elemen Ke-',batasan1+1,' : ',
                    inputan=raw_input()
                    array[batasan1-1]=inputan
            print '----------------------------------------------------------------------------'
            print 'Posisi Antrian Setelah Dilakukan Penginputan'
            print '----------------------------------------------------------------------------'
            print array[0:panjangQ]
            print "Front : ", front ,',', array[front-1],"\nRear  : ", rear,',',array[rear-1]
            lastArrayP=rear
            raw_input('Tekan Sembarang Tombol Untuk Melanjutkan')
            os.system('cls')
            print menu()
        else:
            print "Antrian Telah Dibuat"
            print
            raw_input('Tekan Sembarang Tombol Untuk Melanjutkan')
            os.system('cls')
            print menu()
        
    else:
        print "Antrian Telah Dibuat"
        print
    raw_input('Tekan Sembarang Tombol Untuk Melanjutkan')
    os.system('cls')
    menu()

def insert():
    global front, rear, lastArrayP, panjangQ
    if(panjangQ==0):
        print "Input Panjang Antrian Terlebih Dahulu"
    else:
        if(front==1 and rear==panjangQ or front==rear+1):
            print ("Overflow")
        elif(front==0 and rear==0) :
            front=1
            rear=1
            inputan=raw_input("Masukkan Data :")
            array[lastArrayP]=inputan
            lastArrayP+=1
        else:
            if (rear>=panjangQ):
                rear=1
            else:
                rear+=1

            if(lastArrayP > panjangQ-1):
                lastArrayP=0
                inputan=raw_input("Masukkan Data :")
                array[lastArrayP]=inputan
                lastArrayP+=1
            else:
                inputan=raw_input("Masukkan Data :")
                array[lastArrayP]=inputan
                lastArrayP+=1
        print array[0:panjangQ]
        print "Front : ", front ,',', array[front-1],"\nRear  : ", rear,',',array[rear-1]
    print
    raw_input('Tekan Sembarang Tombol Untuk Melanjutkan')
    os.system('cls')
    menu()

def remove():
    global array, front, rear, lastArrayP
    if(panjangQ==0):
        print "Input Panjang Antrian Terlebih Dahulu"
    else:
        if(front==0 ):
            print ("underflow")
        elif(array[0:panjangQ]==''):
            print ("underflow")
            rear=0
            front=0
            lastArrayP=0
        elif(front==panjangQ):
            array[front-1]=''
            front=1
            lastArrayP=1
        else:
            array[front-1]=''
            front+=1
            lastArrayP+=1
            if(array[rear-1]==''):
                front=0
                rear=0
                lastArrayP=0
        print array[0:panjangQ]
        print "Front : ", front ,',', array[front-1],"\nrear  : ", rear,',',array[rear-1]
    print
    raw_input('Tekan Sembarang Tombol Untuk Melanjutkan')
    os.system('cls')
    menu()

def noel():
    isi=0
    for i  in range(panjangQ):
        if(array[i]!=''):
            isi+=1
        else:
            continue
    print 'Banyak elemen antrian saat ini : ', isi
    raw_input('Tekan Sembarang Tombol Untuk Melanjutkan')
    os.system('cls')
    menu()

def check():
    print array[0:panjangQ]
    print "Front : ", front ,',', array[front-1],"\nrear  : ", rear,',',array[rear-1]
    raw_input('Tekan Sembarang Tombol Untuk Melanjutkan')
    os.system('cls')
    menu()
    
os.system('cls')
menu()
